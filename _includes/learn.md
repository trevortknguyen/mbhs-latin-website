When you meet someone, the first thing you (usually) do is greet them and introduce yourself. If you are talking to one person, you would do this by saying, "Salve! Nomen mihi est *your name*."
